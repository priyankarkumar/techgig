/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require('ask-sdk');
var question =require('./questions.js')


function toss()
{
  let ar=['brother','sister'];
  return ar[Math.floor(ar.length*Math.random())];
}

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    let cSibling=toss();
    let {attributesManager}=handlerInput;
    const speechText= `Welcome! Lets find out who is the real boss at home. Say yes to begin the game. 
                        I am going to toss a coin to decide who goes first. 
                        I have decided ${cSibling} will go first. Say yes and I will begin the game. `;
    let sessionAttributes=attributesManager.getSessionAttributes();
    sessionAttributes['cSibling']=cSibling;
    sessionAttributes['cRound']=0;
    sessionAttributes['brother']=0;
    sessionAttributes['sister']=0;
    attributesManager.setSessionAttributes(sessionAttributes);

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Let us begin', `Say yes to begin`)
      .getResponse();
  },
};

const QuestionIntentHandler={
  canHandle(handlerInput)
  {
    const {request}=handlerInput.requestEnvelope;
    return (request.type==='IntentRequest')&&(request.intent.name==='AMAZON.YesIntent'||request.intent.name===`QuestionIntent`);
  },
  handle(handlerInput)
  {
    const {attributesManager}=handlerInput;
    let sessionAttributes=attributesManager.getSessionAttributes();
    if(!('cSibling' in sessionAttributes))
    {
      const speechText= `Welcome! Lets find out who is the real boss at home. Say yes to begin the game. 
                        I am going to toss a coin to decide who goes first. 
                        I have decided ${cSibling} will go first. Say yes and I will begin the game. `;
    let sessionAttributes=attributesManager.getSessionAttributes();
    sessionAttributes['cSibling']=cSibling;
    sessionAttributes['cRound']=0;
    sessionAttributes['brother']=0;
    sessionAttributes['sister']=0;
    attributesManager.setSessionAttributes(sessionAttributes);

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Let us begin', `Say yes to begin`)
      .getResponse();

    }
    else
    {
      if(sessionAttributes['cSibling'] === 'brother'){
        count = Math.floor(100* Math.random());
        let arrayQuestion = questTrial["questions"];
           let speechText = `Okay! <audio src="https://s3.amazonaws.com/kbm19/ques4.mp3" />`;
           let questionPlay = question[count].question;
           speechText += `<break time="0.50s"/> ${questionPlay}` ;
           speechText += `<break time="0.25s"/>`
           speechText += `A : ${arrayQuestion[count].A} <break time="0.25s"/> B : ${arrayQuestion[count].B} <break time="0.25s"/> C : ${arrayQuestion[count].C} <break time="0.25s"/> D : ${arrayQuestion[count].D} <break time="0.25s"/> Your 30seconds time starts now.  <audio src="https://s3.amazonaws.com/kbm19/input.mp3" />`

      }
      else{
        count = Math.floor(100* Math.random());
        let arrayQuestion = questTrial["questions"];
           let speechText = `Okay! <audio src="https://s3.amazonaws.com/kbm19/ques4.mp3" />`;
           let questionPlay = question[count].question;
           speechText += `<break time="0.50s"/> ${questionPlay}` ;
           speechText += `<break time="0.25s"/>`
           speechText += `A : ${arrayQuestion[count].A} <break time="0.25s"/> B : ${arrayQuestion[count].B} <break time="0.25s"/> C : ${arrayQuestion[count].C} <break time="0.25s"/> D : ${arrayQuestion[count].D} <break time="0.25s"/> Your 30seconds time starts now.  <audio src="https://s3.amazonaws.com/kbm19/input.mp3" />`

      }



    }

  }
};

const AnswerIntentHandler={
  canHandle(handlerInput)
  {
    const {request}=handlerInput.requestEnvelope;
    return (request.type==='IntentRequest')&& request.intent.name===`AnswerIntent`);

  },
  handle(handlerInput)
  {
    const {attributesManager}=handlerInput;
    let sessionAttributes=attributesManager.getSessionAttributes();

    if(sessionAttributes['cRound'] === 0){
      if(sessionAttributes['cSibling'] === 'brother'){
        count--;
        let arrayQuestion = questTrial["questions"];
           let speechText = `Okay! <audio src="https://s3.amazonaws.com/kbm19/ques4.mp3" />`;
           let questionPlay = question[count].question;
           speechText += `<break time="0.50s"/> ${questionPlay}` ;
           speechText += `<break time="0.25s"/>`
           speechText += `A : ${arrayQuestion[count].A} <break time="0.25s"/> B : ${arrayQuestion[count].B} <break time="0.25s"/> C : ${arrayQuestion[count].C} <break time="0.25s"/> D : ${arrayQuestion[count].D} <break time="0.25s"/> Your 30seconds time starts now.  <audio src="https://s3.amazonaws.com/kbm19/input.mp3" />`

      }else{
        let arrayQuestion = questTrial["questions"];
           let speechText = `Okay! <audio src="https://s3.amazonaws.com/kbm19/ques4.mp3" />`;
           let questionPlay = question[count].question;
           speechText += `<break time="0.50s"/> ${questionPlay}` ;
           speechText += `<break time="0.25s"/>`
           speechText += `A : ${arrayQuestion[count].A} <break time="0.25s"/> B : ${arrayQuestion[count].B} <break time="0.25s"/> C : ${arrayQuestion[count].C} <break time="0.25s"/> D : ${arrayQuestion[count].D} <break time="0.25s"/> Your 30seconds time starts now.  <audio src="https://s3.amazonaws.com/kbm19/input.mp3" />`

      }
    }
    else if (sessionAttributes['cRound'] === 1){
      if(sessionAttributes['cSibling'] === 'brother'){
        

      }else{

      }

    }else{
      if(sessionAttributes['cSibling'] === 'brother'){

      }else{

      }

    }

  }
}


const HelpIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    const speechText = 'You can say hello to me!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
        || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    const speechText = 'Goodbye!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

    return handlerInput.responseBuilder.getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('Sorry, I can\'t understand the command. Please say again.')
      .reprompt('Sorry, I can\'t understand the command. Please say again.')
      .getResponse();
  },
};

const skillBuilder = Alexa.SkillBuilders.standard();

exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    SessionEndedRequestHandler
  )
  .withTableName(`Siblings`)
  .withAutoCreateTable(true)
  .lambda();
